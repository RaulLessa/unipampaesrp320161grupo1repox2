package gestaoDeEstagios.bean;

import java.util.GregorianCalendar;

public class TCE {
	String termo = "";
	GregorianCalendar data;
	String cargoRepresentante = "";
	String horaEntrada = "";
	String horaSaida = "";
	String representanteLegal = "";
	String horaIntervalo = "";
	boolean situacaoBolsa = false;
	int valorBolsa;
	String horaSemanal = "";
	int valAuxTransporte;

	public TCE() {

	}

	public GregorianCalendar getData() {
		return data;
	}

	public void setData(GregorianCalendar data) {
		this.data = data;
	}

	public String getCargoRepresentante() {
		return cargoRepresentante;
	}

	public void setCargoRepresentante(String cargoRepresentante) {
		this.cargoRepresentante = cargoRepresentante;
	}

	public String getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(String horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public String getHoraSaida() {
		return horaSaida;
	}

	public void setHoraSaida(String horaSaida) {
		this.horaSaida = horaSaida;
	}

	public String getRepresentanteLegal() {
		return representanteLegal;
	}

	public void setRepresentanteLegal(String representanteLegal) {
		this.representanteLegal = representanteLegal;
	}

	public String getHoraIntervalo() {
		return horaIntervalo;
	}

	public void setHoraIntervalo(String horaIntervalo) {
		this.horaIntervalo = horaIntervalo;
	}

	public boolean isSituacaoBolsa() {
		return situacaoBolsa;
	}

	public void setSituacaoBolsa(boolean situacaoBolsa) {
		this.situacaoBolsa = situacaoBolsa;
	}

	public int getValorBolsa() {
		return valorBolsa;
	}

	public void setValorBolsa(int valorBolsa) {
		this.valorBolsa = valorBolsa;
	}

	public String getHoraSemanal() {
		return horaSemanal;
	}

	public void setHoraSemanal(String horaSemanal) {
		this.horaSemanal = horaSemanal;
	}

	public int getValAuxTransporte() {
		return valAuxTransporte;
	}

	public void setValAuxTransporte(int valAuxTransporte) {
		this.valAuxTransporte = valAuxTransporte;
	}

	public String getTermo() {
		return termo;
	}

	public void setTermo(String termo) {
		this.termo = termo;
	}

	public void gerarTCE() {

	}
}
