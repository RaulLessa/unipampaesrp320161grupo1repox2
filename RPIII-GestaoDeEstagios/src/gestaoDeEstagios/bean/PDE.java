package gestaoDeEstagios.bean;

import java.util.GregorianCalendar;

public class PDE {

	private GregorianCalendar data;
	private String atvddEmpresa = "";
	private String obs = "";
	private String compatibilidade = "";
	private float frequencia;
	private int nFuncionario;
	private int nEstagiarios;
	private int aprendSocial;
	private int aprendProfc;
	private int aprendCult;
	private int saude;
	private int segurancatrab;
	private int superv;
	private boolean status;

	public PDE() {

	}

	public GregorianCalendar getData() {
		return data;
	}

	public void setData(GregorianCalendar data) {
		this.data = data;
	}

	public String getAtvddEmpresa() {
		return atvddEmpresa;
	}

	public void setAtvddEmpresa(String atvddEmpresa) {
		this.atvddEmpresa = atvddEmpresa;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public String getCompatibilidade() {
		return compatibilidade;
	}

	public void setCompatibilidade(String compatibilidade) {
		this.compatibilidade = compatibilidade;
	}

	public float getFrequencia() {
		return frequencia;
	}

	public void setFrequencia(float frequencia) {
		this.frequencia = frequencia;
	}

	public int getnFuncionario() {
		return nFuncionario;
	}

	public void setnFuncionario(int nFuncionario) {
		this.nFuncionario = nFuncionario;
	}

	public int getnEstagiarios() {
		return nEstagiarios;
	}

	public void setnEstagiarios(int nEstagiarios) {
		this.nEstagiarios = nEstagiarios;
	}

	public int getAprendSocial() {
		return aprendSocial;
	}

	public void setAprendSocial(int aprendSocial) {
		this.aprendSocial = aprendSocial;
	}

	public int getAprendProfc() {
		return aprendProfc;
	}

	public void setAprendProfc(int aprendProfc) {
		this.aprendProfc = aprendProfc;
	}

	public int getAprendCult() {
		return aprendCult;
	}

	public void setAprendCult(int aprendCult) {
		this.aprendCult = aprendCult;
	}

	public int getSaude() {
		return saude;
	}

	public void setSaude(int saude) {
		this.saude = saude;
	}

	public int getSegurancatrab() {
		return segurancatrab;
	}

	public void setSegurancatrab(int segurancatrab) {
		this.segurancatrab = segurancatrab;
	}

	public int getSuperv() {
		return superv;
	}

	public void setSuperv(int superv) {
		this.superv = superv;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public void gerarPDE() {

	}

	public void atualizarPDE() {

	}

	public void recuperarDados() {

	}

	public void salvaInfo() {

	}

	public boolean validar() {
		return true;
	}
}
