package gestaoDeEstagios.bean;



public class Oportunidade {
	
	private String nome;
	private String descricao;
	private String atividades;
	private String preRequisitos;
	private boolean permissao = false;
	private boolean estado = true;
	
	public Oportunidade(){
	
	}
	
	public Oportunidade(String nome, String descricao, String atividades, String preRequisitos) {
		super();
		this.nome = nome;
		this.descricao = descricao;
		this.atividades = atividades;
		this.preRequisitos = preRequisitos;
		this.permissao = true;
		this.estado = true;
	}

	public Oportunidade(String nome, String descricao, String atividades, String preRequisitos, boolean estado) {
		super();
		this.nome = nome;
		this.descricao = descricao;
		this.atividades = atividades;
		this.preRequisitos = preRequisitos;
		this.estado = estado;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getAtividades() {
		return atividades;
	}

	public void setAtividades(String atividades) {
		this.atividades = atividades;
	}

	public String getPreRequisitos() {
		return preRequisitos;
	}

	public void setPreRequisitos(String preRequisitos) {
		this.preRequisitos = preRequisitos;
	}

	public boolean isPermissao() {
		return permissao;
	}

	public void setPermissao(boolean permissao) {
		this.permissao = permissao;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
	

	public void getOportunidade() {
		return ;
	}


	@Override
	public String toString() {
		return "Nome: " + nome + "\nDescri�ao: " + descricao + "\nAtividades: " + atividades
				+ "\nPr�-requisitos: " + preRequisitos + "\nEstado: " + estado;
	}
	
	
	

}
