package gestaoDeEstagios.bean;

import java.util.GregorianCalendar;

public class PDA {

	private String atividade;
	private GregorianCalendar data;
	private GregorianCalendar vigInicio;
	private GregorianCalendar vigFim;
	
	public PDA(){
		
	}

	public String getAtividade() {
		return atividade;
	}

	public void setAtividade(String atividade) {
		this.atividade = atividade;
	}

	public GregorianCalendar getData() {
		return data;
	}

	public void setData(GregorianCalendar data) {
		this.data = data;
	}

	public GregorianCalendar getVigInicio() {
		return vigInicio;
	}

	public void setVigInicio(GregorianCalendar vigInicio) {
		this.vigInicio = vigInicio;
	}

	public GregorianCalendar getVigFim() {
		return vigFim;
	}

	public void setVigFim(GregorianCalendar vigFim) {
		this.vigFim = vigFim;
	}
	
	
}
