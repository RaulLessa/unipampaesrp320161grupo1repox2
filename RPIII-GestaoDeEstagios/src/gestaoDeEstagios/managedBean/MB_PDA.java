package gestaoDeEstagios.managedBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import gestaoDeEstagios.bean.PDA;

@ManagedBean
@RequestScoped
public class MB_PDA implements MB_Documento {
	PDA pda;
	private static MB_PDA mbPDA = null;

	// Inicio Singleton
	private MB_PDA() {
		mbPDA = new MB_PDA();
	}

	public static MB_PDA getMB_PDA() {
		if (mbPDA == null) {
			return mbPDA = new MB_PDA();
		} else {
			return mbPDA;

		}
	}
	// Fim Singleton

	@Override
	public void gerarDocumento() {
		// A implementar - sujeito a mudan�as de l�gica e par�metros
	}

	@Override
	public void editarDocumento() {
		// A implementar - sujeito a mudan�as de l�gica e par�metros
	}
}
