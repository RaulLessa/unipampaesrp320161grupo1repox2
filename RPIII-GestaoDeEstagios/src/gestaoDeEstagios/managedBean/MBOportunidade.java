package gestaoDeEstagios.managedBean;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;


import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;


import gestaoDeEstagios.BD.DAOOportunidade;
import gestaoDeEstagios.bean.Oportunidade;


@ManagedBean (name ="MBoportunidade")
@RequestScoped
public class MBOportunidade {

	private Oportunidade oportunidade;
	private ArrayList<Oportunidade> oportunidades;
	
	@PostConstruct
	public void init(){
		this.oportunidade = new Oportunidade();
		this.oportunidades = new ArrayList<>();
	}
	
	
	public void addOportunidade() throws ClassNotFoundException, SQLException{
		DAOOportunidade daoOp = new DAOOportunidade();
		daoOp.inserir(this.oportunidade);
		
	}
	
	public void recuperaOportunidade(){
		
	}



	public Oportunidade getOportunidade() {
		return oportunidade;
	}

	public void voltarHome() throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtmlS");
	}

	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}
	

}
