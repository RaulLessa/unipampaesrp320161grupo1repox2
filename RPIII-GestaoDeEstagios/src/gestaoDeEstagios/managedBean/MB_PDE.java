package gestaoDeEstagios.managedBean;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import gestaoDeEstagios.bean.PDE;

@ManagedBean
@RequestScoped
public class MB_PDE implements MB_Documento {
	PDE pde;
	private static MB_PDE mbPDE = null;

	// Inicio Singleton
	private MB_PDE() {
		mbPDE = new MB_PDE();
	}

	public static MB_PDE getMB_PDE() {
		if (mbPDE == null) {
			return mbPDE = new MB_PDE();
		} else {
			return mbPDE;

		}
	}
	// Fim Singleton

	@Override
	public void gerarDocumento() {
		// A implementar - sujeito a mudan�as de l�gica e par�metros
	}

	@Override
	public void editarDocumento() {
		// A implementar - sujeito a mudan�as de l�gica e par�metros
	}

	public void finalizarPDE() {
		// A implementar - sujeito a mudan�as de l�gica e par�metros
	}

	public void atualizarPDE() {
		// A implementar - sujeito a mudan�as de l�gica e par�metros
	}

	public ArrayList preencherCampos() {
		// A implementar - sujeito a mudan�as de l�gica e par�metros
		return null;
	}

	public void alteraCampos() {
		// A implementar - sujeito a mudan�as de l�gica e par�metros
	}

	public void editar() {
		// A implementar - sujeito a mudan�as de l�gica e par�metros
	}

	public void notificaTela() {
		// A implementar - sujeito a mudan�as de l�gica e par�metros
	}
}
