package gestaoDeEstagios.managedBean;

import gestaoDeEstagios.bean.TCE;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@RequestScoped
public class MB_TCE implements MB_Documento {

	TCE tce;
	private static MB_TCE mbTCE = null;

	// Inicio Singleton
	private MB_TCE() {
		mbTCE = new MB_TCE();
	}

	public static MB_TCE getMB_TCE() {
		if (mbTCE == null) {
			return mbTCE = new MB_TCE();
		} else {
			return mbTCE;

		}
	}
	// Fim Singleton

	@Override
	public void gerarDocumento() {
		// A ser implementado - sujeito a mudan�a de l�gica e par�metros.
		
	}

	@Override
	public void editarDocumento() {
		// A ser implementado - sujeito a mudan�a de l�gica e par�metros.
		
	}

}
