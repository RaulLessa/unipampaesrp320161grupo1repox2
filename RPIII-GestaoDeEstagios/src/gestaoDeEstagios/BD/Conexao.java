package gestaoDeEstagios.BD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {

	public static Connection connection() throws SQLException, ClassNotFoundException{
		Class.forName("org.postgresql.Driver");
		String url = "jdbc:postgresql://localhost:5432/RP3";
		String user = "postgres";
		String password = "rp123";
		Connection connection = DriverManager.getConnection(url, user, password);
		return connection;
	}
	
	public static void main(String[] args)throws ClassNotFoundException, SQLException{
		connection();
	}
	
}
