package gestaoDeEstagios.BD;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import gestaoDeEstagios.bean.Oportunidade;
import gestaoDeEstagios.managedBean.MBOportunidade;

public class DAOOportunidade {
	
	public void inserir(Oportunidade op)throws ClassNotFoundException, SQLException{
		String sql = "INSERT INTO Oportunidade (nome, descricao, atividades, preRequisitos, permissao, estado) VALUES (?,?,?,?,?,?);";
		PreparedStatement ps = Conexao.connection().prepareStatement(sql);
		ps.setString(1, op.getNome());
		ps.setString(2, op.getDescricao());
		ps.setString(3, op.getAtividades());
		ps.setString(4, op.getPreRequisitos());
		ps.setBoolean(5, op.isEstado());
		ps.setBoolean(6, op.isPermissao());
		ps.execute();
		ps.close();
		Conexao.connection().close();
	}

	public void deletar (MBOportunidade mbop) throws ClassNotFoundException, SQLException{
		String sql = "DELETE FROM Oportunidade WHERE ;";
		PreparedStatement ps = Conexao.connection().prepareStatement(sql);
		ps.setObject(1, mbop.getOportunidade());
	}
	
	public void alterar(MBOportunidade mbop){
					
	}
}
